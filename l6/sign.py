from os import urandom

from pyasn1.codec.der import encoder

from structs import SignatureSequence
from common import hasher
import gost


def create_signature(curve, prv, hash):
    signature = gost.sign(curve, prv, hash)
    pub = gost.public_key(curve, prv)
    sign = SignatureSequence()

    params = sign.getComponentByName('params').getComponentByName('keydatasquence')
    openkey = params.getComponentByName('open_key')
    openkey.setComponentByName('x', pub[0])
    openkey.setComponentByName('y', pub[1])

    cryptoparams = params.getComponentByName('cryptosystem_p')
    cryptoparams.setComponentByName('p', curve.p)

    curveparams = params.getComponentByName('curve_p')
    curveparams.setComponentByName('a', curve.a)
    curveparams.setComponentByName('b', curve.b)

    dotsparams = params.getComponentByName('dots_p')
    dotsparams.setComponentByName('x', curve.x)
    dotsparams.setComponentByName('y', curve.y)

    params.setComponentByName('q', curve.q)

    s = sign.getComponentByName('sign')
    s.setComponentByName('r', signature[0])
    s.setComponentByName('s', signature[1])
    return sign


def sign(input_path, result_path, curve, prv):
    with open(input_path, 'rb') as file:
        msg = file.read()
    hash = hasher(msg)
    signature = create_signature(curve, prv, hash)
    with open(result_path, 'wb') as result_file:
        result_file.write(encoder.encode(signature))


if __name__ == '__main__':
    curve_params = gost.CURVE_PARAMS
    curve = gost.Curve(*curve_params)
    prv_raw = urandom(32)
    prv = gost.prv_unmarshal(prv_raw)
    input_name = input()
    output_name = input()
    sign(input_name, output_name, curve, prv)
