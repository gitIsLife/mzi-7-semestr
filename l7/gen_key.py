from ecdsa.keys import SigningKey
import pickle

if __name__ == '__main__':
    filename = input()
    key = SigningKey.generate()

    with open(f'{filename}.pub', 'wb') as pub:
        with open(f'{filename}.priv', 'wb') as priv:
            pickle.dump(key.privkey.secret_multiplier, pub)
            pickle.dump(key.verifying_key.pubkey.point, priv)
