from PIL import Image
import numpy as np

from scipy.fftpack import dct


def dct2(block):
    return dct(dct(block.T, norm='ortho', type=1).T, norm='ortho', type=1)


def map_rgb(block, f):
    return np.dstack((f(block[:, :, 0]), f(block[:, :, 1]), f(block[:, :, 2])))


def bits_to_text(bits):
    chars = []
    for b in range(0, len(bits), 8):
        byte = bits[b: b + 8]
        chars.append(chr(int(byte, 2)))
    return ''.join(chars)


def sign(num):
    return 1 if num >= 0 else -1


if __name__ == "__main__":
    # echo $'hid.jpg\nextracted_msg' | python3 show.py
    hid_name = 'hid.jpg'
    output_name = 'extracted_msg'

    hid_name = input()
    output_name = input()

    np.random.seed(1)

    img = Image.open(hid_name)
    pixels = np.asarray(img).copy()

    x1, y1, c1, x2, y2, c2 = 1, 0, 2, 0, 1, 2

    message_bits = []
    while True:
        i, j = int(np.random.uniform(0, (img.size[1] - 4) // 4)) * 4, int(np.random.uniform(0, (img.size[0] - 4) // 4)) * 4
        if len(message_bits) % 8 == 0 and message_bits and message_bits[-8:] == ['0'] * 8:
            break

        block = pixels[i:i + 4, j:j + 4]
        transformed = map_rgb(block, dct2)

        message_bits.append('1' if abs(transformed[x1, y1, c1]) - abs(transformed[x2, y2, c2]) >= 0 else '0')

    message_bits = ''.join(message_bits)[:-8]

    with open(output_name, 'w') as f:
        print(bits_to_text(message_bits), file=f, end='')
