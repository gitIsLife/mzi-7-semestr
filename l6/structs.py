from pyasn1.type import univ, namedtype

PRETTY_STRING_LENGTH = 100


class OpenKey(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('x', univ.Integer()),
        namedtype.NamedType('y', univ.Integer())
    )


class CryptosystemParams(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('p', univ.Integer())
    )


class CurveParams(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('a', univ.Integer()),
        namedtype.NamedType('b', univ.Integer())
    )


class DotsParams(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('x', univ.Integer()),
        namedtype.NamedType('y', univ.Integer())
    )


class KeyDataSequence(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('open_key', OpenKey()),
        namedtype.NamedType('cryptosystem_p', CryptosystemParams()),
        namedtype.NamedType('curve_p', CurveParams()),
        namedtype.NamedType('dots_p', DotsParams()),
        namedtype.NamedType('q', univ.Integer())
    )


class KeyDataSet(univ.Set):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('keydatasquence', KeyDataSequence())
    )


class SignatureParamsSequence(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('r', univ.Integer()),
        namedtype.NamedType('s', univ.Integer())
    )


class SignatureSequence(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('params', KeyDataSet()),
        namedtype.NamedType('sign', SignatureParamsSequence()),
    )
