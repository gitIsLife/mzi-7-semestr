from PIL import Image
import numpy as np

from scipy.fftpack import dct, idct


def dct2(block):
    return dct(dct(block.T, norm='ortho', type=1).T, norm='ortho', type=1)


def map_rgb(block, f):
    return np.dstack((f(block[:, :, 0]), f(block[:, :, 1]), f(block[:, :, 2])))


def idct2(block):
    return idct(idct(block.T, norm='ortho', type=1).T, norm='ortho', type=1)


def text_to_bits(txt):
    result = []
    for c in txt:
        bits = bin(ord(c))[2:]
        bits = '0' * (8 - len(bits)) + bits
        result.append(bits)
    return ''.join(result)


def sign(num):
    return 1 if num >= 0 else -1


if __name__ == "__main__":
    # echo $'img.jpg\nhid.jpg\nmsg' | python3 hide.py
    img_name = 'img.jpg'
    output_name = 'hid.jpg'
    message_name = 'msg'

    img_name = input()
    output_name = input()
    message_name = input()

    np.random.seed(1)

    img = Image.open(img_name)
    pixels = np.asarray(img).copy()

    with open(message_name, 'r') as f:
        msg = ''.join(f.readlines())

    message_bits = text_to_bits(msg) + '0' * 8
    processed = 0

    x1, y1, c1, x2, y2, c2 = 1, 0, 2, 0, 1, 2

    while processed < len(message_bits):
        i, j = int(np.random.uniform(0, (img.size[1] - 4) // 4)) * 4, int(np.random.uniform(0, (img.size[0] - 4) // 4)) * 4
        bit = message_bits[processed]
        processed += 1

        block = pixels[i:i + 4, j:j + 4]
        transformed = map_rgb(block, dct2)

        diff = abs(transformed[x1, y1, c1]) - abs(transformed[x2, y2, c2])
        if bit == '1':
            while abs(transformed[x1, y1, c1]) - abs(transformed[x2, y2, c2]) <= 50:
                transformed[x1, y1, c1] += 10 * sign(transformed[x1, y1, c1])
                transformed[x2, y2, c2] -= 10 * sign(transformed[x2, y2, c2])
        else:
            while abs(transformed[x1, y1, c1]) - abs(transformed[x2, y2, c2]) >= -50:
                transformed[x2, y2, c2] += 10 * sign(transformed[x2, y2, c2])
                transformed[x1, y1, c1] -= 10 * sign(transformed[x1, y1, c1])

        pixels[i:i + 4, j:j + 4] = map_rgb(transformed, idct2)

    new_img = Image.fromarray(pixels)
    new_img.save(output_name, format='JPEG', quality=100)
