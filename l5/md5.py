from math import sin


class MD5:
    def __init__(self):
        self.shifts = [
            7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
            5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
            4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
            6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21,
        ]

        self.K = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            1, 6, 11, 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12,
            5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15, 2,
            0, 7, 14, 5, 12, 3, 10, 1, 8, 15, 6, 13, 4, 11, 2, 9,
        ]

        self.T = [int(abs(sin(n)) * (1 << 32)) % 2 ** 32 for n in range(1, 65)]

        self.A = 0x67452301
        self.B = 0xefcdab89
        self.C = 0x98badcfe
        self.D = 0x10325476

        self.funcs = (
            lambda x, y, z: (x & y) | (~x & z),
            lambda x, y, z: (z & x) | (~z & y),
            lambda x, y, z: x ^ y ^ z,
            lambda x, y, z: y ^ (~z | x)
        )

    def align(self, msg):
        array = bytearray(msg.encode())
        ln = (8 * len(array)) % 2 ** 64
        array.append(0x80)
        array += (0).to_bytes((64 + 56 - (len(array))) % 64, 'little')
        array += ln.to_bytes(8, 'little')
        return array

    def shift(self, val, shift):
        val %= 2 ** 32
        return ((val << shift) | (val >> (32 - shift))) % 2 ** 32

    def hash(self, array):
        for ind in range(0, len(array), 64):
            word = array[ind:ind + 64]
            A, B, C, D = self.A, self.B, self.C, self.D

            for i in range(64):
                Xk = int.from_bytes(word[self.K[i] * 4: self.K[i] * 4 + 4], 'little')
                F = self.funcs[i // 16](B, C, D)
                A = (B + self.shift(A + F + self.T[i] + Xk, self.shifts[i])) % 2 ** 32
                A, B, C, D = D, A, B, C

            self.A = (self.A + A) % 2 ** 32
            self.B = (self.B + B) % 2 ** 32
            self.C = (self.C + C) % 2 ** 32
            self.D = (self.D + D) % 2 ** 32

        result = sum(x << (32 * i) for i, x in enumerate([self.A, self.B, self.C, self.D]))

        self.__init__()
        return int.from_bytes(result.to_bytes(16, 'big'), 'little')

    def __call__(self, msg):
        array = self.align(msg)
        return self.hash(array)


if __name__ == "__main__":
    h = MD5()
    assert h('md5') == 0x1BC29B36F623BA82AAF6724FD3B16718
    assert h('md4') == 0xC93D3BF7A7C4AFE94B64E30C2CE39F4F
    assert h('') == 0xD41D8CD98F00B204E9800998ECF8427E
