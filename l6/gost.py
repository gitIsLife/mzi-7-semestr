from os import urandom

from common import decode_hex, encode_hex

CURVE_PARAMS_HEX = (
    "8000000000000000000000000000000000000000000000000000000000000431",
    "8000000000000000000000000000000150FE8A1892976154C59CFC193ACCF5B3",
    "0000000000000000000000000000000000000000000000000000000000000007",
    "5FBFF498AA938CE739B8E022FBAFEF40563F6E6A3472FC2A514C0CE9DAE23B7E",
    "0000000000000000000000000000000000000000000000000000000000000002",
    "08E2A8A0E65147D4BD6316030E16D19C85C97F0A9CA267122B96ABBCEA7E8FC8",
)

CURVE_PARAMS = [decode_hex(param) for param in CURVE_PARAMS_HEX]


def deserizlize(raw):
    if isinstance(raw, int):
        return raw
    return int(encode_hex(raw), 16)


def serialize(n, size=32):
    res = hex(int(n))[2:].rstrip("L")
    if len(res) % 2 != 0:
        res = "0" + res
    s = decode_hex(res)
    if len(s) != size:
        s = (size - len(s)) * b"\x00" + s
    return s


def mod_reverse(a, n):
    if a < 0:
        return n - mod_reverse(-a, n)
    t, newt = 0, 1
    r, newr = n, a
    while newr != 0:
        quotinent = r // newr
        t, newt = newt, t - quotinent * newt
        r, newr = newr, r - quotinent * newr
    if r > 1:
        return -1
    if t < 0:
        t = t + n
    return t


class Curve(object):
    def __init__(self, p, q, a, b, x, y):
        self.p = deserizlize(p)
        self.q = deserizlize(q)
        self.a = deserizlize(a)
        self.b = deserizlize(b)
        self.x = deserizlize(x)
        self.y = deserizlize(y)
        r1 = self.y * self.y % self.p
        r2 = ((self.x * self.x + self.a) * self.x + self.b) % self.p
        if r2 < 0:
            r2 += self.p
        assert r1 == r2

    def _pos(self, v):
        if v < 0:
            return v + self.p
        return v

    def _add(self, p1x, p1y, p2x, p2y):
        if p1x == p2x and p1y == p2y:
            t = ((3 * p1x * p1x + self.a) * mod_reverse(2 * p1y, self.p)) % self.p
        else:
            tx = self._pos(p2x - p1x) % self.p
            ty = self._pos(p2y - p1y) % self.p
            t = (ty * mod_reverse(tx, self.p)) % self.p
        tx = self._pos(t * t - p1x - p2x) % self.p
        ty = self._pos(t * (p1x - tx) - p1y) % self.p
        return tx, ty

    def exp(self, degree, x=None, y=None):
        x = x or self.x
        y = y or self.y
        tx = x
        ty = y
        degree -= 1
        if degree == 0:
            raise ValueError("Bad degree value")
        while degree != 0:
            if degree & 1 == 1:
                tx, ty = self._add(tx, ty, x, y)
            degree = degree >> 1
            x, y = self._add(x, y, x, y)
        return tx, ty


def public_key(curve, prv):
    return curve.exp(prv)


def sign(curve, d, hash):
    size = 64
    q = curve.q
    e = deserizlize(hash) % q
    if e == 0:
        e = 1
    while True:
        k = deserizlize(urandom(size)) % q
        if k == 0:
            continue
        r, _ = curve.exp(k)
        r %= q
        if r == 0:
            continue
        d *= r
        k *= e
        s = (d + k) % q
        if s == 0:
            continue
        break
    return r, s


def verify(curve, Q, hash, signature):
    r, s = signature
    q = curve.q
    p = curve.p
    if r <= 0 or r >= q or s <= 0 or s >= q:
        return False
    e = deserizlize(hash) % curve.q
    if e == 0:
        e = 1
    v = mod_reverse(e, q)
    z1 = s * v % q
    z2 = q - r * v % q
    p1x, p1y = curve.exp(z1)
    q1x, q1y = curve.exp(z2, Q[0], Q[1])
    lmbd = (q1x - p1x + p) % p
    lmbd = mod_reverse(lmbd, p)
    lmbd = lmbd * (q1y - p1y) % p
    lmbd = lmbd * lmbd % p
    xc = (lmbd - p1x - q1x + p) % p
    R = xc % q
    return R == r


def prv_unmarshal(prv):
    return deserizlize(prv[::-1])


def pub_marshal(pub):
    size = 64
    return (serialize(pub[1], size) + serialize(pub[0], size))[::-1]


def pub_unmarshal(pub):
    size = 64
    pub = pub[::-1]
    return deserizlize(pub[size:]), deserizlize(pub[:size])
