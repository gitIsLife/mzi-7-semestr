from pyasn1.codec.der import decoder

from structs import SignatureSequence
from common import hasher
import gost


def verify_signature(hash, struct):
    params = struct.getComponentByName('params').getComponentByName('keydatasquence')

    openkey_p = params.getComponentByName('open_key')
    pub = int(openkey_p.getComponentByName('x')), int(openkey_p.getComponentByName('y'))

    p = int(params.getComponentByName('cryptosystem_p').getComponentByName('p'))
    q = int(params.getComponentByName('q'))
    a = int(params.getComponentByName('curve_p').getComponentByName('a'))
    b = int(params.getComponentByName('curve_p').getComponentByName('b'))
    x = int(params.getComponentByName('dots_p').getComponentByName('x'))
    y = int(params.getComponentByName('dots_p').getComponentByName('y'))

    curve = gost.Curve(p, q, a, b, x, y)

    signature = int(struct.getComponentByName('sign').getComponentByName('r')), int(
        struct.getComponentByName('sign').getComponentByName('s'))

    return gost.verify(curve, pub, hash, signature)


def verify_file(msg_name, sign_name):
    with open(msg_name, 'rb') as file, open(sign_name, 'rb') as sign_f:
        data = file.read()
        struct, _ = decoder.decode(sign_f.read(), asn1Spec=SignatureSequence())
    hash = hasher(data)
    return verify_signature(hash, struct)


if __name__ == '__main__':
    msg_name = input()
    sign_name = input()
    print(f'File is{" not" if not verify_file(msg_name, sign_name) else ""} OK')
