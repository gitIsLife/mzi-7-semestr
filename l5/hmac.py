from md5 import MD5


def _hmac(key, block):
    ipad = int('36' * 64, base=16)
    opad = int('5c' * 64, base=16)
    h = MD5()
    return h(hex(opad ^ key)[2:] + hex(h(hex(ipad ^ key)[2:] + block))[2:])


def hmac(key_str, msg):
    key = int.from_bytes(key_str.encode(), 'big')
    result = []
    for i in range(0, len(msg), 64):
        block = msg[i: i + 64]
        result.append(hex(_hmac(key, block)))
    return ''.join(result)


if __name__ == "__main__":
    key = input()
    msg = input()
    print(hmac(key, msg))
