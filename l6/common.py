from codecs import getdecoder
from codecs import getencoder


_hexdecoder = getdecoder("hex")
_hexencoder = getencoder("hex")


def decode_hex(data):
    return _hexdecoder(data)[0]


def encode_hex(data):
    return _hexencoder(data)[0].decode("ascii")


def hasher(data):
    from stribog import Stribog
    dgst = Stribog(hash_size=256)
    dgst.update(data)
    return dgst.hash()