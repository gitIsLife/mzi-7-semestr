import pickle

if __name__ == '__main__':
    private = input()
    public = input()

    with open(public, 'rb') as pub:
        with open(private, 'rb') as priv:
            public = pickle.load(pub)
            private = pickle.load(priv)

    print(public * private)
